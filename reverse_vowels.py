import logging
logging.basicConfig(level=logging.INFO, format='%(levelname)s - %(message)s')


class Solution:

    def __init__(self):
        self.vowels_list = ['a', 'e', 'i', 'o', 'u']

    def reverseVowels(self, s: str) -> str:
        """
        :type s: str
        :rtype : str
        """

        # Create ordered list of vowels in string
        vowels_in_string_list = []
        for char in s:
            if char.lower() in self.vowels_list:
                vowels_in_string_list.append(char)

        # Reverse vowels_list
        vowels_in_string_list.reverse()

        vowels_in_string_list_cnt = 0
        return_str = ''
        for c in s:
            if c.lower() in self.vowels_list:
                return_str += vowels_in_string_list[vowels_in_string_list_cnt]
                vowels_in_string_list_cnt += 1
            else:
                return_str += c

        logging.info(return_str)

        return return_str


if __name__=="__main__":
    mySolution=Solution()
    return_str=mySolution.reverseVowels('aA')
    logging.info(return_str)