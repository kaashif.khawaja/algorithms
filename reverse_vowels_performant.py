import logging

logging.basicConfig(level=logging.INFO, format='%(levelname)s - %(message)s')


class Solution:

    def reverseVowels(self, s: str) -> str:
        """
        :type s: str
        :rtype : str
        """
        vowels = set("aeiouAEIOU")
        i, j = 0, len(s) - 1
        s = list(s)
        while i < j:
            if s[i] in vowels and s[j] in vowels:
                temp = s[i]
                s[i], s[j] = s[j], temp
                i += 1
                j -= 1

            if s[i] not in vowels:
                i += 1
            if s[j] not in vowels:
                j -= 1

        return ''.join(s)


if __name__ == "__main__":
    mySolution = Solution()
    reverse_vowels=mySolution.reverseVowels('Aa')
    logging.info(reverse_vowels)
