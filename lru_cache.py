class DLinkedListNode():
    def __init__(self, key, value):
        self.key = key
        self.value = value
        self.prev = None
        self.next = None

class LRUCache():
    def __init__(self, capacity):
        self.capacity = capacity
        self.size = 0
        self.cache = {}
        self.head, self.tail = DLinkedListNode('HEAD', 'HEAD'), DLinkedListNode('TAIL', 'TAIL')
        self.head.next = self.tail
        self.tail.prev = self.head

    def _add_node(self, node):
        """
         Insert after head
        :param node:
        :return: None
        """

        node.prev = self.head
        node.next = self.head.next

        self.head.next.prev = node
        self.head.next = node

    def _remove_node(self,node):
        """
        Remove node from end of linked list
        :param node:
        :return:
        """

        prev = node.prev
        next = node.next

        prev.next = next
        next.prev = prev

    def _pop_tail(self):
        """
        Pop the tail
        :return:
        """

        res = self.tail.prev
        self._remove_node(res)
        return res

    def _move_to_head(self, node):
        self._remove_node(node)
        self._add_node(node)

    def get(self, key):
        """
        :param key: int
        :return: int
        """

        node = self.cache.get(key, None)

        if not node:
            return -1

        self._move_to_head(node)
        return node.value

    def put(self, key, value):
        """
        :param key: int
        :param value: int
        :return:
        """

        node = self.cache.get(key)

        if not node:
            newnode = DLinkedListNode(key, value)
            self.cache[key] =newnode
            self._add_node(newnode)

            self.size +=1

            if self.size > self.capacity:
                tail = self._pop_tail()
                del self.cache[tail.key]
                self.size -=1
        else:
            node.value = value
            self._move_to_head(node)


if __name__ == "__main__":
    lRUCache = LRUCache(2)
    lRUCache.put(1, 1)
    lRUCache.put(2, 2)
    lRUCache.get(1)
    lRUCache.put(3, 3)
    lRUCache.get(2)
    lRUCache.put(4, 4)
    lRUCache.get(1)
    lRUCache.get(3)
    lRUCache.get(4)