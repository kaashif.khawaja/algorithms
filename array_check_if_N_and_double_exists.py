class Solution(object):
    def checkIfExist(self, arr):
        """
        :type arr: List[int]
        :rtype: bool
        """
        s = set()
        for num in arr:
            if num in s:
                return True
            else:
                if num % 2 == 0:
                    s.add(int(num/2))
                s.add(int(num*2))
        return False



if __name__ == "__main__":
    soln = Solution()
    nums = [3,1,7,11,22]
    result=soln.checkIfExist(nums)
    print(result)
