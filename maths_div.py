class Solution:
    def div(self, a: int, b: int) -> int:
        sum = 0
        count = 0
        while sum < a:
            sum +=b
            count +=1
        return count

if __name__ == "__main__":
    my_Solution = Solution()
    print(my_Solution.div(100, 5))