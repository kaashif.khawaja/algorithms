# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:
    def removeElements(self, head: ListNode, val: int) -> ListNode:

        dummy = ListNode(0)
        dummy.next = head

        prev = dummy
        cur = head

        while cur:
            if cur.val == val:
                prev.next = cur.next
            else:
                prev = cur
            cur = cur.next

        return dummy.next


if __name__ == "__main__":
    node_1 = ListNode(3)
    node_2 = ListNode(2)
    node_3 = ListNode(3)
    node_4 = ListNode(4)
    node_5 = ListNode(5)

    node_1.next = node_2
    node_2.next = node_3
    node_3.next = node_4
    node_4.next = node_5
    node_5.next = None

    head = node_1
    while head:
        print(head.val)
        head = head.next if head else None

    soln = Solution()
    head = soln.removeElements(node_1, 3)

    print("========================")
    while head:
        print(head.val)
        head = head.next if head else None
