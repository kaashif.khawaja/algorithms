class Solution:
    def sum_digits(self, a: int) -> int:
        sum = 0
        while a > 0:
            digit = a % 10
            print(f"Value of digit = {digit}")
            sum += digit
            a = a // 10
            print(f"Value of number = {a}")

        return sum

if __name__ == "__main__":
    my_Solution = Solution()
    print(my_Solution.sum_digits(123))