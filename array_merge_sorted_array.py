class Solution:
    def merge_sorted_array(self, nums1: list[int], m: int, nums2: list[int], n: int) -> None:
        p1 = m - 1
        p2 = n - 1
        pos = m + n - 1
        while p1 >= 0 and p2 >= 0:
            if nums1[p1] < nums2[p2]:
                nums1[pos] = nums2[p2]
                p2 -= 1
            else:
                nums1[pos] = nums1[p1]
                p1 -= 1
            pos -= 1

        nums1[:p2 + 1] = nums2[:p2 + 1]


if __name__ == "__main__":
    mySolution = Solution()

    nums1 = [2,0]
    m = 1
    nums2 = [1]
    n = 1

    #nums1=[0]
    #m=0
    #nums2=[1]
    #n=1
    mySolution.merge_sorted_array(nums1, m, nums2, n)
    print(nums1)
