class Solution:
    def power_of(self, a: int, b: int) -> int:
        if b < 0:
            return 0
        elif b == 0:
            return 1
        else:
            return a*self.power_of(a, b-1)

if __name__ == "__main__":
    my_Solution = Solution()
    print(my_Solution.power_of(2, 3))