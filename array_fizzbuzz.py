from typing import List

class Solution:
    def fizzBuzz(self, n: int) -> List[str]:
        result = []
        for count in range(1, n + 1):
            if count % 3 ==0 and count % 5 == 0:
                result.append("FizzBuzz")
            elif count % 3 == 0:
                result.append("Fizz")
            elif count % 5 == 0:
                result.append("Buzz")
            else:
                result.append(str(count))
        return result


if __name__ == "__main__":
    my_soln = Solution()
    result = my_soln.fizzBuzz(15)
    print(result)