from collections import deque


def stack_using_list():
    queue = []

    for i in range(100):
        queue.append(i)

    for i in range(100):
        print(queue.pop())


def queue_using_collections_deque():
    de = deque([])
    for i in range(100):
        de.append(i)

    for i in range(100):
        print(de.pop())


if __name__ == "__main__":
    stack_using_list()