class Solution:
    def calculateTime(self, keyboard: str, word: str) -> int:
        key_board_map = {}
        for index, ch in enumerate(keyboard):
            key_board_map[ch] = index

        time_taken, current = 0, 0
        for ch in word:
            time_taken += abs(key_board_map[ch] - current)
            current = key_board_map[ch]
        return time_taken




if __name__ == "__main__":
    my_sol = Solution()
    my_sol.calculateTime("pqrstuvwxyzabcdefghijklmno", "leetcode")