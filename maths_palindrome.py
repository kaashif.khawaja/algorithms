class Solution:
    def palindrone(self, s: str) -> int:
        new_string = "".join([char for char in s if char.isalpha() or char.isdigit()])
        return new_string.lower() == new_string[::-1].lower()

if __name__ == "__main__":
    my_Solution = Solution()
    print(my_Solution.palindrone("0P"))