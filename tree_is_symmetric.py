from typing import List


# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def isSymmetric(self, root: TreeNode) -> bool:
        if root is None:
            return True

        def check(L: TreeNode, R: TreeNode):
            if L is None and R is None:
                return True
            elif (L is None and R) or (L and R is None) or L.val != R.val:
                return False
            elif L is not None and R is not None:
                return check(L.left, R.right) and check(L.right, R.left)
            else:
                return False

        return check(root.left, root.right)

if __name__ == "__main__":
    root = TreeNode(val=1)
    node_1_1 = TreeNode(val=2)
    node_1_2 = TreeNode(val=3)
    node_1_3 = TreeNode(val=4)

    root.left = node_1_1
    node_1_1.left = node_1_2
    node_1_1.right = node_1_3

    node_2_1 = TreeNode(val=2)
    node_2_2 = TreeNode(val=4)
    node_2_3 = TreeNode(val=3)

    root.right = node_2_1
    node_2_1.left = node_2_2
    node_2_1.right = node_2_3

    soln = Solution()
    result = soln.isSymmetric(root)
    print(result)
