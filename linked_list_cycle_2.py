# Definition for singly-linked list.
class ListNode:
     def __init__(self, x):
         self.val = x
         self.next = None

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution:
    def detectCycle(self, head: ListNode) -> ListNode:
        visited = set()

        current = head
        while current and current.next:
            if current in visited:
                return current
            else:
                visited.add(current)
            current = current.next
        return None



if __name__ == "__main__":
    node_1 = ListNode(1)
    #node_2 = ListNode(2)

    node_1.next = None
    #node_2.next = node_1
    my_solution = Solution()
    print(my_solution.detectCycle(node_1))
