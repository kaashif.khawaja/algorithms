# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


class Solution:
    def isPalindrome(self, head: ListNode) -> bool:
        vals = []

        cur = head
        while cur:
            vals.append(cur.val)
            cur = cur.next

        return vals == list(reversed(vals))


node_1 = ListNode(1)
node_2 = ListNode(2)
node_3 = ListNode(2)
node_4 = ListNode(1)

node_1.next = node_2
node_2.next = node_3
node_3.next = node_4
node_4.next = None

head = node_1
while head:
    print(head.val)
    head = head.next if head else None

soln = Solution()
print(soln.isPalindrome(node_1))
