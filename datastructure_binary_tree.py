from typing import List

#               1
#           /       \
#          2           3
#        /   \       /   \
#      4      5     6    7
#
#
# Definition for a binary tree node.
class TreeNode:
     def __init__(self, val=0, left=None, right=None):
         self.val = val
         self.left = left
         self.right = right


class BinaryTree():
    def pre_order_traversal(self, node):

        if node:
            print(f"Value of node = {node.val}")
            self.pre_order_traversal(node.left)
            self.pre_order_traversal(node.right)

    def in_order_traversal(self, node):

        if node:
            self.pre_order_traversal(node.left)
            print(f"Value of node = {node.val}")
            self.pre_order_traversal(node.right)

    def post_order_traversal(self, node):

        if node:
            self.pre_order_traversal(node.left)
            self.pre_order_traversal(node.right)
            print(f"Value of node = {node.val}")

    @classmethod
    def breadth_first_traversal(cls, node):

        queue = [node]

        while len(queue):
            node = queue.pop(0)
            if node.left is not None:
                queue.append(node.left)
            if node.right is not None:
                queue.append(node.right)
            print(f"Value of node = {node.val}")

    #               1
    #           /       \
    #          2           3
    #        /   \       /   \
    #      4      5     6    7
    #
    #

    def route_to_leaf_sum(self, node, sum : int, route : List):
        if node.left is None and node.right is None:
            if node.val == sum:
                route.append(node.val)
                return True
            else:
                return False
        if self.route_to_leaf_sum(node.left, sum - node.val, route):
            route.append(node.val)
            return True
        if self.route_to_leaf_sum(node.right, sum-node.val, route):
            route.append(node.val)
            return True
        return False

    def height_of_tree(self, node : TreeNode) -> int:
        if not node:
            return 0

        left_height = self.height_of_tree(node.left)
        right_height = self.height_of_tree(node.right)

        return 1 + max(left_height, right_height)

if __name__ == "__main__":
    root = TreeNode(val=1)
    node_2 = TreeNode(val=2)
    node_4 = TreeNode(val=4)
    node_5 = TreeNode(val=5)

    node_3 = TreeNode(val=3)
    node_6 = TreeNode(val=6)
    node_7 = TreeNode(val=7)

    root.left = node_2
    root.right = node_3

    node_2.left = node_4
    node_2.right = node_5

    node_3.left = node_6
    node_3.right = node_7

    soln = BinaryTree()
    print("============PRE ORDER TRAVERSAL============")
    soln.pre_order_traversal(root)
    print("\n")
    print("============IN ORDER TRAVERSAL============")
    soln.in_order_traversal(root)
    print("\n")
    print("============POST ORDER TRAVERSAL============")
    soln.post_order_traversal(root)
    print("============BREADTH FIRST TRAVERSAL============")
    print("\n")
    soln.breadth_first_traversal(root)

    print("============ROUTE TO LEAF SUM==================")
    route = []
    sum = 11
    result = soln.route_to_leaf_sum(root, sum, route)
    print(route)

    print("============HEIGHT OF TREE==================")
    result = soln.height_of_tree(root)
    print(result)
