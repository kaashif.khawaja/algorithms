class Solution(object):
    def validMountainArray(self, arr):
        """
        :type arr: List[int]
        :rtype: bool
        """
        if len(arr) < 3:
            return False

        up = down = False
        for i in range(1, len(arr)):
            if arr[i] > arr[i-1] and not down:
                up = True
            elif arr[i] < arr[i-1] and up:
                down = True
            else:
                return False

        return up & down


if __name__ == "__main__":
    soln = Solution()
    nums = [3,5, 5]
    result = soln.validMountainArray(nums)
    print(result)