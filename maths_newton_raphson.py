
def sqrt_helper(n : int, min_x : int, max_x :int) ->int:
        guess = (min_x + max_x)/2
        if guess * guess == n:
            return guess
        elif guess * guess > n:
            return sqrt_helper(n, min_x, guess-1)
        else:
            return sqrt_helper(n, guess+1, max_x)

def sqrt(n : int) -> int:
    return sqrt_helper(n, 1, n)

if __name__ == "__main__":
    print(sqrt(100))