class Solution:
    def isPalindrome(self, x: int) -> bool:
        return list(str(x)) == list(reversed(str(x)))


if __name__ == "__main__":
    soln = Solution()
    result = soln.isPalindrome(122)
    print(result)
