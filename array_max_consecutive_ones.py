from typing import List

class Solution:
    def findMaxConsecutiveOnes(self, nums: List[int]) -> int:
        longest_sequence = 0
        left, right = 0, 0
        no_of_zeroes = 0

        while right < len(nums):
            if nums[right]==0:
                no_of_zeroes +=1

            while no_of_zeroes == 2:
                if nums[left]==0:
                    no_of_zeroes -=1
                left +=1

            longest_sequence = max(longest_sequence, right - left + 1)
            right +=1

        return longest_sequence


if __name__ == "__main__":
    soln = Solution()
    nums=[1, 0, 1, 1, 0]




    result = soln.findMaxConsecutiveOnes(nums)
    print(result)