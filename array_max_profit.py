A = [310, 315, 275, 295, 260, 270, 290, 230, 255, 250]  # Max profit is 30: buy 260 and sell 290


def max_prof_not_good(A):
    max_prof = 0
    l = len(A)

    for i in range(l - 1):
        for j in range(i + 1, l):
            profit = A[j] - A[i]
            if profit > max_prof:
                max_prof = profit
    return max_prof


def max_prof_optimal(A):
    min_price = A[0]
    max_profit = 0.0

    for num in A:
        min_price = min(num, min_price)

        compare = num - min_price

        max_profit = max(max_profit, compare)

    return max_profit


if __name__ == "__main__":
    print(max_prof_optimal(A))
