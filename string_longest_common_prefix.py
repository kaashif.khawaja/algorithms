from typing import List

class Solution:
    def longestCommonPrefix(self, strs: List[str]) -> str:

        prefix = ""
        l = len(strs)
        if l:
            min_string_len = len(min(strs))

            for i in range(min_string_len):
                local_prefix = strs[0][i]
                for j in range(1,l):
                    if local_prefix is not strs[j][i]:
                        return prefix
                prefix += local_prefix
        return prefix




if __name__ == "__main__":
    soln = Solution()
    strs = ["flower","flow","flight"]
    strs = ["dog","racecar","car"]
    result = soln.longestCommonPrefix(strs)
    print(result)