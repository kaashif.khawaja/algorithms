import logging

class Solution:
    def arrayStringsareEqual(self, word1: list[str], word2: list[str]) -> bool:
        return "".join(word1) == "".join(word2)

if __name__ == "__main__":
    mySolution = Solution()
    print(mySolution.arrayStringsareEqual(["ab", "c"], ["a", "bc"]))