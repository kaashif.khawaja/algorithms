class Solution:
    def mod_operator(self, a: int, b: int) -> int:
        div = a // b
        return a - div * 10


if __name__ == "__main__":
    my_Solution = Solution()
    print(my_Solution.mod_operator(89, 10))
