from typing import List

class Solution:
    def sortArrayByParity(self, A: List[int]) -> List[int]:
        return [x for x in A if x % 2 == 0] + [x for x in A if x % 2 == 1]

if __name__ == "__main__":
    soln = Solution()
    nums = [3,1,2,4]
    result = soln.sortArrayByParity(nums)
    print(result)