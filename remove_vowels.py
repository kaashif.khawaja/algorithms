import logging


class Solution(object):

    def __init__(self):
        self.vowels_list: list = ['a', 'e', 'i', 'o', 'u']

    def removeVowels(self, S) -> str:
        """
        :type S: str
        :rtype: str
        """
        return_string = ""
        for char in S:
            if char not in self.vowels_list:
                    return_string+=char

        return return_string


if __name__=="__main__":
    mySolution = Solution()
    without_vowels=mySolution.removeVowels("leetcodeisacommunityforcoders")
    logging.info(without_vowels)
    assert without_vowels=="ltcdscmmntyfrcdrs"