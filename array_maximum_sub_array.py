from typing import List
from sys import maxsize

class Solution:
    def maxSubArray(self, nums: List[int]) -> int:
        currentSum = maxSoFar = nums[0]

        for idx in range(1, len(nums)):
            currentSum = max(nums[idx], currentSum + nums[idx])
            maxSoFar = max(maxSoFar, currentSum)

        return maxSoFar


if __name__ == "__main__":
    soln = Solution()
    nums = [-2,1]
    result = soln.maxSubArray(nums)
    print(result)
