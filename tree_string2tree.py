# Definition for a binary tree node.

class TreeNode:
     def __init__(self, val=0, left=None, right=None):
         self.val = val
         self.left = left
         self.right = right

class Solution:

    def getNumber(self, s, index):

        is_negative = False

        if s[index] == "-":
            is_negative = True
            index +=1

        number = 0
        while index < len(s) and s[index].isdigit():
            number += 10 * number + int(s[index])
            index +=1

        return number * -1 if is_negative else number, index


    def str2tree(self, s) -> TreeNode:
        if s is None:
            return None
        root = TreeNode()
        stack = [root]

        index = 0
        while index < len(s):
            node = stack.pop()

            if s[index].isdigit() or s[index] == '-':
                number, index = self.getNumber(s, index)
                node.val = number

                if s[index] == "(":
                    stack.append(node)

                    node.left = TreeNode()
                    stack.append(node.left)
            elif node.left and s[index] == "(":
                stack.append(node)
                node.right = TreeNode
                stack.append(node.right)
            index +=1
        return root

    def pre_order_traversal(self, node):

        if node:
            print(f"Value of node = {node.val}")
            self.pre_order_traversal(node.left)
            self.pre_order_traversal(node.right)

if __name__ == "__main__":
    soln = Solution()
    str = "4(2(3)(1))(6(5))"
    root = soln.str2tree(str)
    print(root)
    soln.pre_order_traversal(root)
