class Solution():

        INT_MAX = 2 ** 31 - 1
        INT_MIN = -2 ** 31

        def reverse(self, to_reverse: int) -> int:
            is_negative = True if to_reverse < 0 else False
            to_reverse = -1 * to_reverse if is_negative else to_reverse

            reverse = 0
            while to_reverse > 0:
                last_digit = to_reverse % 10

                if reverse > Solution.INT_MAX / 10 or reverse == Solution.INT_MAX / 10 and last_digit > 7:
                    return 0

                if reverse < Solution.INT_MIN / 10 or reverse == Solution.INT_MIN / 10 and last_digit > 8:
                    return 0

                reverse = (reverse * 10) + last_digit
                to_reverse = to_reverse // 10

            return -1 * reverse if is_negative else reverse


if __name__ == "__main__":
    my_solution = Solution()
    print(my_solution.reverse(-29233))
