
class Solution:

    def __init__(self):
        self.map_single = { "0" : "0",
                            "8" : "8",
                            "1" : "1",
                            "0" : "0"}

        self.map_sentence = {"6": "9",
                             "9": "6" }

    def isStrobogrammatic2(self, num: str) -> bool:

        if len(num) == 1:
            if num in self.map_single:
                return True
            else:
                return False

        map = self.map_sentence | self.map_single

        l = len(num)
        m = l /2 if l % 2 == 0 else l //2
        iteration = len(num) // 2

        for i in range(iteration):
            if num[int(m)] not in map:
                return False
            elif num[i] not in map:
                return False
            elif num[l - i - 1] != map[num[i]]:
                return False

        return True

    def isStrobogrammatic(self, num: str) -> bool:
        self.map = {"0": "0",
                    "8": "8",
                    "1": "1",
                    "0": "0",
                    "6": "9",
                    "9": "6"}

        reverse_arr = []
        for n in num:
            if n not in self.map:
                return False
            else:
                reverse_arr.append(self.map[n])
        reverse_str = "".join(reversed(reverse_arr))
        return num == reverse_str





if __name__ == "__main__":
    soln = Solution()
    result = soln.isStrobogrammatic("1")
    print(result)
