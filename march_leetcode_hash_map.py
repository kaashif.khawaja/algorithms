class Bucket:
    def __init__(self):
        self.bucket = []

    def get(self, key: int) -> int:
        for (k, v) in self.bucket:
            if k == key:
                return v
        return -1

    def update(self, key: int, value: int) -> None:
        found = False
        for i, kv in enumerate(self.bucket):
            if kv[0] == key:
                self.bucket[i] = (key, value)
                found = True
                break

        if not found:
            self.bucket.append((key, value))

    def remove(self, key: int):
        for i, kv in enumerate(self.bucket):
            if kv[0] == key:
                del self.bucket[i]

class MyHashMap:

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.length = 2069
        self.buckets = [Bucket() for i in range(self.length)]

    def put(self, key: int, value: int) -> None:
        """
        value will always be non-negative.
        """
        hash_value = key % self.length
        self.buckets[hash_value].update(key, value)

    def get(self, key: int) -> int:
        """
        Returns the value to which the specified key is mapped, or -1 if this map contains no mapping for the key
        """
        hash_value = key % self.length
        return self.buckets[hash_value].get(key)

    def remove(self, key: int) -> None:
        """
        Removes the mapping of the specified value key if this map contains a mapping for the key
        """
        hash_value = key % self.length
        return self.buckets[hash_value].remove(key)


# Your MyHashMap object will be instantiated and called as such:
hashMap = MyHashMap();
hashMap.put(1, 1);
hashMap.put(2, 2);
print(hashMap.get(1))  # returns 1
print(hashMap.get(3))  # returns -1 (not found)
hashMap.put(2, 1);  # update the existing value
print(hashMap.get(2))  # returns 1
hashMap.remove(2);  # remove the mapping for 2
print(hashMap.get(2))  # returns -1 (not found)
