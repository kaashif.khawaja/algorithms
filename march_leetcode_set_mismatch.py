from typing import List

class Solution:
    def findErrorNums(self, nums: List[int]) -> List[int]:

        missing = [idx+1 for idx in range(len(nums)) if idx+1 not in nums]

        dups = [None]
        for elem in nums:
            if nums[abs(elem)-1] < 0:
                dups[0]=abs(elem)
            else:
                nums[abs(elem)-1] *= -1

        return dups+missing

    def findErrorNums_2(self, nums: List[int]) -> List[int]:
        n = len(nums)
        s = sum(nums)
        c = sum(set(nums))
        d = n*(n+1)/2

        return [s-c, int(d-c)]

if __name__ == "__main__":
    my_soln = Solution()
    nums = [1,2,2,4]
    print(my_soln.findErrorNums_2(nums))
