from typing import List

class Solution:
    def missingNumber(self, nums: List[int]) -> int:
        n = len(nums)
        s = int(n * (n+1) / 2)
        return s - sum(nums)

if __name__ == "__main__":
    my_soln = Solution()
    print(my_soln.missingNumber([0,1]))