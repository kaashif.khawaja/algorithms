# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


class Solution:
    def removeNthFromEnd(self, head: ListNode, n: int) -> ListNode:

        dummy = ListNode(0)
        dummy.next = head

        length = 0
        ptr = head
        while ptr:
            length += 1
            ptr = ptr.next

        cur = dummy
        length -=n
        while length>0:
            length -=1
            cur = cur.next

        cur.next = cur.next.next
        return dummy.next


if __name__ == "__main__":
    node_1 = ListNode(1)
    node_2 = ListNode(2)
    node_3 = ListNode(3)
    node_4 = ListNode(4)
    node_5 = ListNode(5)

    node_1.next = node_2
    node_2.next = node_3
    node_3.next = node_4
    node_4.next = node_5
    node_5.next = None

    print("===================")

    node_1 = ListNode(1)
    node_1.next = None

    ptr = node_1
    while ptr:
        print(f"{ptr.val}")
        ptr = ptr.next if ptr.next else None


    soln = Solution()
    head = soln.removeNthFromEnd(node_1, 1)

    ptr = head
    while ptr:
        print(f"{ptr.val}")
        ptr = ptr.next if ptr.next else None
