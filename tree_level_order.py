from typing import List


# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def levelOrder(self, root: TreeNode) -> List[List[int]]:

        if root is None:
            return []

        levels = []
        level = 0

        queue = [root]

        while len(queue):

            levels.append([])
            level_length = len(queue)

            for i in range(level_length):
                node = queue.pop(0)
                levels[level].append(node.val)

                if node.left is not None:
                    queue.append(node.left)
                if node.right is not None:
                    queue.append(node.right)
            # go to next level
            level += 1

        return levels

if __name__ == "__main__":
    root = TreeNode(val=3)
    node_2 = TreeNode(val=2)
    node_3 = TreeNode(val=1)

    root.right = node_2
    node_2.left = node_3

    soln = Solution()
    result = soln.levelOrder(root)
    print(result)