# Definition for singly-linked list.
class ListNode:
     def __init__(self, val=0, next=None):
         self.val = val
         self.next = next

class Solution:
    def mergeTwoLists(self, l1, l2):
        if l1 is None:
            return l2
        elif l2 is None:
            return l1
        elif l1.val < l2.val:
            l1.next = self.mergeTwoLists(l1.next, l2)
            return l1
        else:
            l2.next = self.mergeTwoLists(l1, l2.next)
            return l2


if __name__ == "__main__":
    node_1_1 = ListNode(1)
    node_1_2 = ListNode(2)
    node_1_3 = ListNode(4)

    node_1_1.next = node_1_2
    node_1_2.next = node_1_3
    node_1_3.next = None

    node_2_1 = ListNode(1)
    node_2_2 = ListNode(3)
    node_2_3 = ListNode(4)

    node_2_1.next = node_2_2
    node_2_2.next = node_2_3
    node_2_3.next = None

    my_solution = Solution()
    head = my_solution.mergeTwoLists(node_1_1, node_2_1)

    while head:
        print(head.val)
        head = head.next if head else None
