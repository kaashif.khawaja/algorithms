class Solution:
    def defangIpadd(self, address : str) -> str:
        return address.replace('.', '[.]')