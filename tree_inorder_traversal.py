from typing import List


# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def inorderTraversal(self, root: TreeNode) -> List[int]:
        if not root:
            return []

        return self.inorderTraversal(root.left) + [root.val] + self.inorderTraversal(root.right)


if __name__ == "__main__":
    root = TreeNode(val=1)
    node_2 = TreeNode(val=2)
    node_3 = TreeNode(val=3)

    root.right = node_2
    node_2.left = node_3

    soln = Solution()
    result = soln.inorderTraversal(root)
    print(result)
