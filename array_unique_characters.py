"""
RUN THIS CELL TO TEST YOUR CODE>
"""
from nose.tools import assert_equal

def uni_char(string):
    return len(string)==len(set(string))

def uni_char2(string):

    l = len(string)
    if(l == 0):
        return True
    if(l==1):
        return True

    i = 0
    my_dict = dict()

    while i < l:
        if string[i] not in my_dict:
            my_dict[string[i]] = 0
        else:
            return False
        i+=1

    return True

class TestUnique(object):

    def test(self, sol):
        assert_equal(sol(''), True)
        assert_equal(sol('goo'), False)
        assert_equal(sol('abcdefg'), True)
        print("ALL TEST CASES PASSED")


# Run Tests
t = TestUnique()
t.test(uni_char)