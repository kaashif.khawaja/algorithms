

class Solution:
    def duplicating_zeroes(self, arr: list[int]) -> None:
        i = 1
        while i < len(arr):
            if arr[i-1]==0:
                arr[i], arr[i+1:] = 0, arr[i:-1]
                i+=2
            else:
                i+=1



if __name__ == "__main__":
    mySoluton = Solution()
    test_arr = [1,0,2,3,0,4,5,0]
    mySoluton.duplicating_zeroes(test_arr)
    print(test_arr)
