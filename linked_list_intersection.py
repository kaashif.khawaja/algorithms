# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None


class Solution:
    def getIntersectionNode(self, headA: ListNode, headB: ListNode) -> ListNode:

        if headA == headB:
            return headA

        len_a, len_b = 1, 1

        curr_a = headA
        while curr_a and curr_a.next:
            len_a += 1
            curr_a = curr_a.next

        curr_b = headB
        while curr_b and curr_b.next:
            len_b += 1
            curr_b = curr_b.next

        move_fwd = len_a - len_b
        if move_fwd> 0:
            for i in range(move_fwd):
                headA = headA.next
        else:
            move_fwd = abs(move_fwd)
            for i in range(move_fwd):
                headB = headB.next

        while headA and headB:
            if headA == headB:
                return headA
            else:
                headA, headB = (headA.next if headA.next else None), (headB.next if headA.next else None)
        return None


if __name__ == "__main__":
    node_1_1 = ListNode(3)
    #node_1_2 = ListNode(9)
    #node_1_3 = ListNode(1)
    #node_1_4 = ListNode(2)
    #node_1_5 = ListNode(4)

    node_1_1.next = None
    #node_1_2.next = node_1_3
    #node_1_3.next = node_1_4
    #node_1_4.next = node_1_5
    #node_1_5.next = None

    node_2_1 = ListNode(2)

    node_2_1.next = node_1_1
    my_solution = Solution()
    intersection = my_solution.getIntersectionNode(node_1_1, node_2_1)
    print(intersection.val if intersection is not None else "No intersection")
