from typing import List


class Solution:
    def countConsistentStrings(self, allowed: str, words: List[str]) -> int:

        allowed_dict = {}

        for ch in allowed:
            allowed_dict[ch] = ch

        consisted_srings = 0

        for word in words:
            consisted_srings += 1
            for ch in word:
                if ch not in allowed_dict:
                    consisted_srings -= 1
                    break
        return consisted_srings

if __name__ =="__main__":
    allowed = "ab"
    words = ["ad", "bd", "aaab", "baa", "badab"]
    my_soln = Solution()
    print(my_soln.countConsistentStrings(allowed, words))