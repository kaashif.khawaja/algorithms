class Solution:
    def fib(self, n: int) -> int:

        # n    = 0, 1, 2, 3, 4, 5, 6, 7,  8
        # f(n) = 0, 1, 1, 2, 3, 5, 8, 13, 21

        if n == 0:
            return 0
        if n <= 2:
            return 1

        fn_2 = 0
        fn_1 = 1

        for index in range(2, n+1):
            fn = fn_2 + fn_1
            fn_2 = fn_1
            fn_1 = fn

        return fn

if __name__ == "__main__":
    mySolution = Solution()
    print(mySolution.fib(7))