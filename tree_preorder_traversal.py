from typing import List

# Definition for a binary tree node.
class TreeNode:
     def __init__(self, val=0, left=None, right=None):
         self.val = val
         self.left = left
         self.right = right

class Solution:
    def preorderTraversal(self, root: TreeNode) -> List[int]:
        output = []
        if root:
            stack  = [root]

            while len(stack):
                node = stack.pop()
                output.append(node.val)

                if node.left is not None:
                    stack.append(node.left)
                if node.right is not None:
                    stack.append(node.right)
        return output

    def preorderTraversal_Recursive(self, root: TreeNode) -> List[int]:
        if root is None:
            return []

        return [root.val] + self.preorderTraversal(root.left) + self.preorderTraversal(root.right)


if __name__ == "__main__":
    root = TreeNode(val=3)
    node_2 = TreeNode(val=2)
    node_3 = TreeNode(val=1)

    root.right = node_2
    node_2.left = node_3

    soln = Solution()
    result = soln.preorderTraversal(root)
    print(result)


