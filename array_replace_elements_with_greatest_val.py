class Solution(object):
    def replaceElements(self, arr):
        """
        :type arr: List[int]
        :rtype: List[int]
        """
        max_till_now = arr[len(arr)-1]
        arr[len(arr) - 1] = -1
        for j in range(len(arr)-2,-1, -1):

            temp = arr[j]
            arr[j] = max_till_now

            if temp > max_till_now:
                max_till_now = temp

        return arr



if __name__ == "__main__":
    soln = Solution()
    nums = [17,18,5,4,6,1]
    nums = [400]
    #nums = [57010,40840,69871,14425,70605]

    print(nums)
    print("====================")
    result=soln.replaceElements(nums)
    print(result)