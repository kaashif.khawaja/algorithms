"""
RUN THIS CELL TO TEST YOUR SOLUTION
"""
from nose.tools import assert_equal


def compress(to_compress):
    l = len(to_compress)

    if l == 0:
        return ''

    if l == 1:
        return to_compress + '1'

    result = ''
    j = 1
    i = 1
    ch = to_compress[i]
    while i < l:
        new_ch = to_compress[i]
        if new_ch == ch:
            j +=1
        else:
            result += ch
            if j != 1:
                result += str(j)
            ch = to_compress[i]
            j = 1
        i += 1

    result += ch + str(j)
    return result





class TestCompress(object):

    def test(self, sol):
        assert_equal(sol(''), '')
        assert_equal(sol('AABBCC'), 'A2B2C2')
        assert_equal(sol('AAABCCDDDDD'), 'A3BC2D5')
        assert_equal(sol('AAAAABBBBCCCC'), 'A5B4C4')


        print("ALL TEST CASES PASSED")



def main():
    # Run Tests
    t = TestCompress()
    t.test(compress)


if __name__ == "__main__":
    main()