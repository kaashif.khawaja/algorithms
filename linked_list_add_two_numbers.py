# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


class Solution(object):
    def addTwoNumbers(self, l1, l2):
        """
        :type l1: ListNode
        :type l2: ListNode
        :rtype: ListNode
        """
        l1_cur = l1
        l2_cur = l2
        l1_val = l1_cur.val
        l2_val = l2_cur.val
        carry = 0
        dummy_head = ListNode(-1)
        l3 = dummy_head
        while l1_cur is not None or l2_cur is not None:
            val = l1_val + l2_val + int(carry)
            sum = val % 10
            new_node = ListNode(sum)
            l3.next = new_node
            l3 = new_node
            carry = (val - sum) / 10

            l1_cur = l1_cur.next if l1_cur else None
            l2_cur = l2_cur.next if l2_cur else None

            l1_val = l1_cur.val if l1_cur else 0
            l2_val = l2_cur.val if l2_cur else 0

        if carry:
            new_node = ListNode(int(carry))
            l3.next = new_node

        return dummy_head.next


if __name__ == "__main__":
    node_1_1 = ListNode(9)
    node_1_2 = ListNode(9)
    node_1_3 = ListNode(9)
    node_1_4 = ListNode(9)
    node_1_5 = ListNode(9)
    node_1_6 = ListNode(9)
    node_1_7 = ListNode(9)

    node_1_1.next = node_1_2
    node_1_2.next = node_1_3
    node_1_3.next = node_1_4
    node_1_4.next = node_1_5
    node_1_5.next = node_1_6
    node_1_6.next = node_1_7
    node_1_7.next = None

    node_2_1 = ListNode(9)
    node_2_2 = ListNode(9)
    node_2_3 = ListNode(9)
    node_2_4 = ListNode(9)

    node_2_1.next = node_2_2
    node_2_2.next = node_2_3
    node_2_3.next = node_2_4
    node_2_4.next = None
    my_solution = Solution()
    head = my_solution.addTwoNumbers(node_1_1, node_2_1)

    while head:
        print(head.val)
        head = head.next if head else None
