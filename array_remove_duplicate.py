from typing import List


class Solution:
    def removeDuplicates(self, nums: List[int]) -> int:
        if len(nums):
            return 0

        j = 0
        for i in range(1, len(nums)):
            if nums[i] is not nums[j]:
                j += 1
                nums[j] = nums[i]
        return j + 1


if __name__ == "__main__":
    soln = Solution()
    nums = [-50,-50,-49,-48,-47,-47,-47,-46]
    result = soln.removeDuplicates(nums)
    print(f"{nums} and result = {result}")
    print("[-50, -49, -48, -47, -46, -45, -43, -42,-41,-40,-39,-38,-37,-36,-35,-34,-33,-32,-31,-30,-28,-27,-26")
