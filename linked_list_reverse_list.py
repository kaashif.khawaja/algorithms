# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None


class Solution:
    def reverseList(self, head: ListNode) -> ListNode:
        prev = None
        cur = head

        while cur:
            next = cur.next
            cur.next = prev
            prev = cur
            cur = next

        return prev


if __name__ == "__main__":
    node_1_1 = ListNode(3)
    node_1_2 = ListNode(9)
    node_1_3 = ListNode(1)
    node_1_4 = ListNode(2)
    node_1_5 = ListNode(4)

    node_1_1.next = node_1_2
    node_1_2.next = node_1_3
    node_1_3.next = node_1_4
    node_1_4.next = node_1_5

    head = node_1_1

    while head:
        print(head.val)
        head = head.next if head else None

    soln = Solution()
    head = soln.reverseList(node_1_1)

    print("========================")
    while head:
        print(head.val)
        head = head.next if head else None
