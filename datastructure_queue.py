from collections import deque


def queue_using_list():
    queue = []

    for i in range(100):
        queue.append(i)

    for i in range(100):
        print(queue.pop(0))


def queue_using_collections_deque():
    de = deque([])
    for i in range(100):
        de.append(i)

    for i in range(100):
        print(de.popleft())


if __name__ == "__main__":
    # queue_using_list()
    queue_using_collections_deque()
