# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None


class Solution:
    def hasCycle(self, head: ListNode) -> bool:

        if head is None or not head.next:
            return False

        slow_ptr = head
        fast_ptr = head.next

        while slow_ptr != fast_ptr:
            if slow_ptr is None or fast_ptr is None:
                return False
            elif slow_ptr.next is None or fast_ptr.next is None:
                return False
            slow_ptr, fast_ptr = slow_ptr.next, fast_ptr.next.next

        return True


if __name__ == "__main__":
    node_1 = ListNode(1)
    node_2 = ListNode(2)

    node_1.next = node_2
    node_2.next = None
    my_solution = Solution()
    print(my_solution.hasCycle(node_1))
