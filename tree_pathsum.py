from typing import List


# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def hasPathSum(self, root: TreeNode, targetSum: int) -> bool:

        if root is None:
            return False

        if root.left is None and root.right is None:
            if root.val == targetSum:
                return True

        if root.left is not None:
            if self.hasPathSum(root.left, targetSum - root.val):
                return True
        if root.right is not None:
            if self.hasPathSum(root.right, targetSum - root.val):
                return True

        return False


if __name__ == "__main__":
    root = TreeNode(val=3)
    node_2 = TreeNode(val=2)
    node_3 = TreeNode(val=1)

    root.right = node_2
    node_2.left = node_3

    soln = Solution()
    result = soln.hasPathSum(root, 6)
    print(result)
