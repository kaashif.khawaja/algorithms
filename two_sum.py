"""
Complexity Analysis:

Time complexity : O(n). We traverse the list containing nn elements only once. Each look up in the table costs only O(1) time.

Space complexity : O(n). The extra space required depends on the number of items stored in the hash table, which stores at most nn elements.

"""
class Solution:
    def twoSum(self, nums: list[int], target: int) -> list[int]:
        num_map = dict()

        for i, num in enumerate(nums):
            complement = target-num

            if complement in num_map:
               return [i, num_map[complement]]

            num_map[num]=i

        raise ValueError('Compement does not exist')

if __name__ == "__main__":
    mySolution=Solution()
    print(mySolution.twoSum([2,7,11,15], 9))
