
class Solution():

    def reverseString(self, s: list[str]) -> None:

        i,j=0,len(s)-1

        while i < j:
            temp=s[i]
            s[i],s[j]=s[j],temp
            i+=1
            j-=1

        return s

if __name__=="__main__":
    mySolution=Solution()
    print( mySolution.reverseString(["h","e","l","l","o"]))

