class Solution(object):
    def moveZeroes(self, nums):
        """
        :type nums: List[int]
        :rtype: None Do not return anything, modify nums in-place instead.
        """
        if len(nums) == 0:
            return nums

        i = 0
        j = 1
        while i < len(nums) and j < len(nums):
            if nums[i] != 0:
                i += 1
                j += 1
                continue

            if nums[j] == 0:
                j += 1
                continue

            if nums[i] == 0 and nums[j] != 0:
                temp = nums[i]
                nums[i] = nums[j]
                nums[j] = temp
            i += 1
            j += 1

        return nums


if __name__ == "__main__":
    soln = Solution()
    nums = [0, 1, 0, 3, 12]
    #nums = [0]
    #nums = [0,1,0,1,0,1,0,1]
    #nums = [4,2,4,0,0,3,0,5,1,0]
    result = soln.moveZeroes(nums)
    print(result)
